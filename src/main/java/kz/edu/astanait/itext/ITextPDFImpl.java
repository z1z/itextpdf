package kz.edu.astanait.itext;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.property.VerticalAlignment;
import kz.edu.astanait.dto.IndividualPlanDto;
import kz.edu.astanait.models.Course;
import kz.edu.astanait.models.Student;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ITextPDFImpl implements ITextPDF {
    private static final String PATH = "D:" + File.separator + "Индивидуальный план.pdf";
    private static final String FONT_PATH = "src" + File.separator + "main" + File.separator + "resources"
            + File.separator + "times.ttf";
    private static final String AITU_LOGO_PATH = "src" + File.separator + "main" + File.separator + "resources"
            + File.separator + "Astana IT University.png";

    private int courseNumber = 0;

    @Override
    public void createPDF(IndividualPlanDto dto) {
        courseNumber = Calendar.getInstance().get(Calendar.YEAR) + 1 - dto.getStudent().getYear();
        try {
            PdfWriter pdfWriter = new PdfWriter(PATH);

            PdfDocument pdfDocument = new PdfDocument(pdfWriter);

            Document document = new Document(pdfDocument, PageSize.A4);
            float documentMargin = 14f;
            document.setMargins(documentMargin, documentMargin, documentMargin, documentMargin);

            PdfFont font = PdfFontFactory.createFont(FONT_PATH, PdfEncodings.IDENTITY_H);
            document.setFont(font);
            document.setFontSize(6);

            createHeader(document);
            createMiddle(document, dto.getStudent());
            createTable(document, dto.getStudent(), dto.getCourses());

            document.close();
            System.out.println("Done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createHeader(Document document) {
        UnitValue[] columnWidth = {
                UnitValue.createPercentValue(40),
                UnitValue.createPercentValue(20),
                UnitValue.createPercentValue(40)
        };
        Table table = new Table(columnWidth)
                .setWidth(UnitValue.createPercentValue(100))
                .setBorderBottom(new SolidBorder(0.5f));

        Image aituLogo = null;
        try {
            ImageData aituLogoData = ImageDataFactory.create(AITU_LOGO_PATH);
            aituLogo = new Image(aituLogoData).setHeight(100f);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        table.addCell(
                new Cell().add(new Paragraph("ТОО \"Astana IT University\""))
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setBorder(Border.NO_BORDER)
        );

        table.addCell(new Cell().add(aituLogo).setBorder(Border.NO_BORDER));

        table.addCell(
                new Cell().add(new Paragraph("ЖШС \"Astana IT University\""))
                        .setTextAlignment(TextAlignment.RIGHT)
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setBorder(Border.NO_BORDER)
        );

        document.add(table);
    }

    private void createMiddle(Document document, Student student) {
        Paragraph paragraph = new Paragraph()
                .setTextAlignment(TextAlignment.RIGHT);

        paragraph.add("Утверждаю\n")
                .add("Декан\n")
                .add("Astana IT University\n")
                .add("Сергазиев Муслим Жаксылыкович\n")
                .add("_________________________\n")
                .add(new Text("(подпись)\n")
                        .setItalic())
                .add("___  ___________20__г\n")
                .add(new Text("(печать)\n")
                        .setItalic());
        document.add(paragraph);

        document.add(new Paragraph("Индивидуальный план учебный план")
                .setFontSize(10)
                .setTextAlignment(TextAlignment.CENTER));

        paragraph = new Paragraph();
        paragraph
                .add(new Text("Обучающийся " + student.getName() + "\n")
                        .setUnderline())
                .add(new Text("Фамилия Имя Отчество\n")
                        .setItalic())
                .add(new Text("Академическая степень Бакалавр\n")
                        .setUnderline())
                .add(new Text("Номер транскрипта\n")
                        .setUnderline())
                .add(new Text("Группа образовательных программ " + student.getOp() + "\n")
                        .setUnderline())
                .add(new Text("Наименование группы образовательной программы (шифр)\n")
                        .setItalic())
                .add(new Text("Тема дипломной работы []\n")
                        .setUnderline())
                .add(new Text("Форма обучения Очное (бакалавр 3 года), 3 г.\n")
                        .setUnderline())
                .add(new Text("Наименование, количество лет обучения\n")
                        .setItalic());

        paragraph.add(new Text("Курс " + courseNumber + "\n")
                .setUnderline());

        paragraph.add(new Text(Calendar.getInstance().get(Calendar.YEAR) + " - "
                + (Calendar.getInstance().get(Calendar.YEAR) + 1) + " учебный год")
                .setUnderline());

        document.add(paragraph);
    }

    private void createTable(Document document, Student student, List<Course> courses) {
        UnitValue[] columnWidth = {
                UnitValue.createPercentValue(1),//N
                UnitValue.createPercentValue(4),//OK\VK
                UnitValue.createPercentValue(10),//Code
                UnitValue.createPercentValue(30),//Name
                UnitValue.createPercentValue(7),//Credits
                UnitValue.createPercentValue(10),//TypeClass.type
                UnitValue.createPercentValue(28),//TypeClass.teacher
                UnitValue.createPercentValue(4),//TypeClass.hours
                UnitValue.createPercentValue(6)//TypeControl
        };
        Table table = new Table(columnWidth);

        Style style = new Style()
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setHorizontalAlignment(HorizontalAlignment.CENTER)
                .setTextAlignment(TextAlignment.CENTER);
        table.addCell(new Cell().add(new Paragraph("№").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("ОК/\nВК/\nКВ/\nДВО/\nУПП").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Код дисциплины").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Название").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Количество кредитов").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Вид занятия").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Преподователь").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Часы").addStyle(style)));
        table.addCell(new Cell().add(new Paragraph("Форма контроля").addStyle(style)));

        table.addCell(new Cell(0, table.getNumberOfColumns())
                .add(new Paragraph(courseNumber
                        + " Курс обучения " + Calendar.getInstance().get(Calendar.YEAR) + " - "
                        + (Calendar.getInstance().get(Calendar.YEAR) + 1)
                        + " учебный год")
                        .setTextAlignment(TextAlignment.CENTER)));

        Integer minTrimester = courses.get(0).getTrimester();
        Integer maxTrimester = courses.get(0).getTrimester();
        for (Course course : courses) {
            minTrimester = Math.min(course.getTrimester(), minTrimester);
            maxTrimester = Math.max(course.getTrimester(), maxTrimester);
        }

        Integer totalCredits = 0;
        for (int i = minTrimester; i <= maxTrimester; i++) {
            int finalI = i;
            AtomicInteger n = new AtomicInteger(1);
            List<Course> coursesInTrimester = courses.stream()
                    .filter(course -> course.getTrimester().equals(finalI))
                    .collect(Collectors.toCollection(LinkedList::new));

            table.addCell(new Cell(0, table.getNumberOfColumns())
                    .add(new Paragraph(i + " Триместр")
                            .setTextAlignment(TextAlignment.CENTER)
                    ));
            Integer totalCreditsInTrimester = 0;
            for (Course course : coursesInTrimester) {
                totalCreditsInTrimester += course.getCredit();
                table.addCell(new Cell(course.getTypeClass().size(), 0)
                        .add(new Paragraph(String.valueOf(n.getAndIncrement()))
                                .setTextAlignment(TextAlignment.CENTER)
                        ));
                table.addCell(new Cell(course.getTypeClass().size(), 0).add(new Paragraph(course.getType())
                        .setTextAlignment(TextAlignment.CENTER)));
                table.addCell(new Cell(course.getTypeClass().size(), 0).add(new Paragraph(course.getCode())));
                table.addCell(new Cell(course.getTypeClass().size(), 0).add(new Paragraph(course.getName())));
                table.addCell(new Cell(course.getTypeClass().size(), 0)
                        .add(new Paragraph(String.valueOf(course.getCredit()))
                                .setTextAlignment(TextAlignment.CENTER)));

                final AtomicBoolean isSet = new AtomicBoolean(false);
                course.getTypeClass().forEach(typeClass -> {
                    table.addCell(new Cell().add(new Paragraph(typeClass.getType())));
                    table.addCell(new Cell().add(new Paragraph(typeClass.getTeacher())));
                    table.addCell(new Cell().add(new Paragraph(String.valueOf(typeClass.getHours()))
                            .setTextAlignment(TextAlignment.CENTER)));
                    if (!isSet.get()) {
                        table.addCell(new Cell(course.getTypeClass().size(), 0)
                                .add(new Paragraph(course.getTypeControl())));
                        isSet.set(true);
                    }
                });
            }
            table.addCell(new Cell());
            table.addCell(new Cell(0, 3).add(new Paragraph("Общее количество кредитов")));
            table.addCell(new Cell().add(new Paragraph(String.valueOf(totalCreditsInTrimester))
                    .setTextAlignment(TextAlignment.CENTER)));
            table.addCell(new Cell());
            table.addCell(new Cell());
            table.addCell(new Cell());
            table.addCell(new Cell());
            totalCredits += totalCreditsInTrimester;
        }
        table.addCell(new Cell());
        table.addCell(new Cell(0, 3).add(new Paragraph("Общее количество кредитов за курс")));
        table.addCell(new Cell().add(new Paragraph(String.valueOf(totalCredits))
                .setTextAlignment(TextAlignment.CENTER)));
        table.addCell(new Cell());
        table.addCell(new Cell());
        table.addCell(new Cell());
        table.addCell(new Cell());

        document.add(table);

        columnWidth = new UnitValue[]{
                UnitValue.createPercentValue(50),//N
                UnitValue.createPercentValue(50)
        };
        Table lastTable = new Table(columnWidth);
        lastTable.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("Регистратор:")));
        lastTable.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("________________________________________ 20__г.")));
        lastTable.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("Обучающийся: " + student.getName() + ":")));
        lastTable.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("________________________________________ 20__г.")));
        document.add(lastTable);
    }
}
