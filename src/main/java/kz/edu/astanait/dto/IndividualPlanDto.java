package kz.edu.astanait.dto;

import kz.edu.astanait.models.Course;
import kz.edu.astanait.models.Student;

import java.util.List;

public class IndividualPlanDto {
    private Student student;
    private List<Course> courses;

    public IndividualPlanDto(){}

    public IndividualPlanDto(Student student, List<Course> courses) {
        this.student = student;
        this.courses = courses;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}