package kz.edu.astanait;

import kz.edu.astanait.dto.IndividualPlanDto;
import kz.edu.astanait.itext.ITextPDF;
import kz.edu.astanait.itext.ITextPDFImpl;
import kz.edu.astanait.models.Course;
import kz.edu.astanait.models.Student;
import kz.edu.astanait.models.TypeClass;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Course> courses = new LinkedList<>();
        courses.add(new Course(4, "OK","Fiz 2116", "Физическая культура", 2,
                Arrays.asList(
                        new TypeClass("Практики, Семинары", "Джемалов Аскар Ислямович", 20),
                        new TypeClass("Самостоятельная работа студента и преподователя", "Джемалов Аскар Ислямович", 10)
                ),
                "Экзамен"
        ));
        courses.add(
                new Course(4, "OK", "MSP 2315", "Политология", 2,
                        Arrays.asList(
                                new TypeClass("Лекции", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Практики, Семинары", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Самостоятельная работа студента и преподователя", "Джампеисова Жанар Махметовна", 10)
                        ), "Экзамен"));

        courses.add(
                new Course(5, "OK","MSP 2315", "Политология", 2,
                        Arrays.asList(
                                new TypeClass("Лекции", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Практики, Семинары", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Самостоятельная работа студента и преподователя", "Джампеисова Жанар Махметовна", 10)
                        ), "Экзамен"));

        courses.add(
                new Course(6, "VP","MSP 2315", "Политология", 2,
                        Arrays.asList(
                                new TypeClass("Лекции", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Практики, Семинары", "Джампеисова Жанар Махметовна", 10),
                                new TypeClass("Самостоятельная работа студента и преподователя", "Джампеисова Жанар Махметовна", 10)
                        ), "Экзамен"));


        IndividualPlanDto dto = new IndividualPlanDto();
        dto.setCourses(courses);
        dto.setStudent(new Student("Сейтмагамбетов Азизбек Сулейменович", "SE", 2019));

        ITextPDF iTextPDF = new ITextPDFImpl();
        iTextPDF.createPDF(dto);
    }
}
