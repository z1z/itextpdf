package kz.edu.astanait.models;

public class TypeClass {
    private String type;
    private String teacher;
    private Integer hours;

    public TypeClass(String type, String teacher, Integer hours) {
        this.type = type;
        this.teacher = teacher;
        this.hours = hours;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }
}