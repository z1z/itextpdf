package kz.edu.astanait.itext;

import kz.edu.astanait.dto.IndividualPlanDto;

public interface ITextPDF {
    void createPDF(IndividualPlanDto dto);
}
