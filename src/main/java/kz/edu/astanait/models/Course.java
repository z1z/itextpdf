package kz.edu.astanait.models;

import java.util.List;

public class Course {
    private Integer trimester;
    private String type;
    private String code;
    private String name;
    private Integer credit;
    private List<TypeClass> typeClass;
    private String typeControl;

    public Course(Integer trimester, String type, String code, String name, Integer credit, List<TypeClass> typeClass, String typeControl) {
        this.trimester = trimester;
        this.type = type;
        this.code = code;
        this.name = name;
        this.credit = credit;
        this.typeClass = typeClass;
        this.typeControl = typeControl;
    }

    public Integer getTrimester() {
        return trimester;
    }

    public void setTrimester(Integer trimester) {
        this.trimester = trimester;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public List<TypeClass> getTypeClass() {
        return typeClass;
    }

    public void setTypeClass(List<TypeClass> typeClass) {
        this.typeClass = typeClass;
    }

    public String getTypeControl() {
        return typeControl;
    }

    public void setTypeControl(String typeControl) {
        this.typeControl = typeControl;
    }
}
