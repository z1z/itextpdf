package kz.edu.astanait.models;

public class Student {
    private String name;
    private String op;
    private Integer year;

    public Student(String name, String op, Integer year) {
        this.name = name;
        this.op = op;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
